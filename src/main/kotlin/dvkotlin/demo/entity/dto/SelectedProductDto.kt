package dvkotlin.demo.entity.dto

data class SelectedProductDto(
        var quantity: Int?=null,
        var product: ProductDto?=null
)