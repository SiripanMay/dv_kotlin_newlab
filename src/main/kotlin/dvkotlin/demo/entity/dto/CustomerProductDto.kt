package dvkotlin.demo.entity.dto

import dvkotlin.demo.entity.ShoppingCartStatus

data class CustomerProductDto (
        var customer: CustomerDto?=null
)