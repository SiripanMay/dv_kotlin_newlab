package dvkotlin.demo.entity.dto

import dvkotlin.demo.entity.ShoppingCart


data class PageShoppingCartDto(
        var totalPages:Int?=null,
        var totalElements:Long?=null,
        var shoppingCarts: List<ShoppingCartDto> = mutableListOf()
)