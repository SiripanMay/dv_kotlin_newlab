package dvkotlin.demo.repository

import dvkotlin.demo.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository:CrudRepository<SelectedProduct,Long>{
    fun findByProduct_NameContainingIgnoreCase(name: String): List<SelectedProduct>
    fun findByProduct_NameContainingIgnoreCase(name: String,page:Pageable): Page<SelectedProduct>
}
