package dvkotlin.demo.repository

import dvkotlin.demo.entity.Customer
import dvkotlin.demo.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository:CrudRepository<Customer,Long>{
    fun findByName(name:String):Customer?
    fun findByNameEndingWith(name:String):List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name:String,email:String):List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(province:String):List<Customer>
    fun findByUserStatus(status: UserStatus):List<Customer>
    fun findByIsDeletedIsFalse():List<Customer>
}