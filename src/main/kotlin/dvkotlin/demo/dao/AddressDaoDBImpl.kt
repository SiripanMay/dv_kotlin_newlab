package dvkotlin.demo.dao

import dvkotlin.demo.entity.Address
import dvkotlin.demo.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoDBImpl:AddressDao{


    override fun findById(id: Long): Address? {
        return addressRepository.findById(id).orElse(null)
    }

    override fun save(add: Address): Address {
        return addressRepository.save(add)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun getAddresses(): List<Address> {
        return addressRepository.findAll().filterIsInstance(Address::class.java)
    }
}

