package dvkotlin.demo.util


import dvkotlin.demo.entity.*
import dvkotlin.demo.entity.dto.*
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "sprong")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
@Mappings(
        Mapping(source = "manufacturer", target = "manu"),
        Mapping(source = "amountInStock", target = "amountInt")
)
    fun mapProductDto(product:Product?): ProductDto?
    @InheritInverseConfiguration
    fun mapProductDto(products:List<Product>):List<ProductDto>
    fun mapManufacturer(manu: Manufacturer):ManufacturerDto

    fun mapCustomerDto(customer: Customer?):CustomerDto?
    fun mapCustomerDto(customer: List<Customer>):List<Customer>


    fun mapShoppingCartDto(shoppingCart: ShoppingCart):ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>):List<ShoppingCartDto>

    fun mapSelectedProductDto(selectedProduct: SelectedProduct):SelectedProductDto
    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>):List<SelectedProductDto>

    fun mapCustomerProductDto(shoppingCart: ShoppingCart):CustomerProductDto
    fun mapCustomerProductDto(shoppingCarts: List<ShoppingCart>):List<CustomerProductDto>

    fun mapAddress(address: Address):AddressDto

    @InheritInverseConfiguration
    fun mapManufacturer(manu:ManufacturerDto):Manufacturer

    @InheritConfiguration
    fun mapAddress(address:AddressDto):Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto):Product

    @InheritConfiguration
    fun mapCustomerDto(customerDto: CustomerDto):Customer

//    @InheritConfiguration
//    fun mapShoppingCartDto(shoppingCartDto: ShoppingCartDto):ShoppingCart
}