package dvkotlin.demo.servive

import dvkotlin.demo.entity.Address
import dvkotlin.demo.entity.dto.AddressDto


interface AddressService{
    fun getAddress():List<Address>
    abstract fun save(address: AddressDto): Address

}