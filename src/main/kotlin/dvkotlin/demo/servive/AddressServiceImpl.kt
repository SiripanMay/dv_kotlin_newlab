package dvkotlin.demo.servive

import dvkotlin.demo.dao.AddressDao
import dvkotlin.demo.entity.Address
import dvkotlin.demo.entity.dto.AddressDto
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl:AddressService{
    override fun save(address: AddressDto): Address {
        val add =MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(add)
    }

    override fun getAddress(): List<Address> {
        return addressDao.getAddresses()
    }

    @Autowired
    lateinit var addressDao: AddressDao
}