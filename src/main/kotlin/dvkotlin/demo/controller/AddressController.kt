package dvkotlin.demo.controller

import dvkotlin.demo.entity.dto.AddressDto
import dvkotlin.demo.servive.AddressService
import dvkotlin.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController{
    @Autowired
    lateinit var addressService: AddressService

    @PostMapping("/address")
    fun addAddress(@RequestBody address:AddressDto):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(address)))
    }

    @PutMapping("/address/{addId}")
    fun  updateAddress(@PathVariable("addId")id:Long?,
                       @RequestBody address: AddressDto):ResponseEntity<Any>{
        address.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(address)))
    }

}